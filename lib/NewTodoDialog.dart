import 'package:flutter/material.dart';
import './Todo.dart';

class NewTodoDialog extends StatelessWidget{

  Widget build(BuildContext context){
    final TextEditingController addTodoController = new TextEditingController();

    return AlertDialog(
          title : Text("Add a todo"),
          content : TextField(
            controller : addTodoController,
            autofocus : true
          ),
          actions : <Widget>[
            FlatButton(
              child : Text("Add"),
              onPressed : (){
                  final todo = Todo(title : addTodoController.value.text);
                  addTodoController.clear();
                  Navigator.of(context).pop(todo);
              }
            ),
            FlatButton(
              child: Text("Cancel"),
              onPressed : (){
                Navigator.of(context).pop();
              }
            )
          ]
        );
  }
}