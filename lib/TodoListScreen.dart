import 'package:flutter/material.dart';
import './Todo.dart';
import './NewTodoDialog.dart';
import './TodoList.dart';

class TodoListScreen extends StatefulWidget{
  @override
  _TodoListScreenState createState() => _TodoListScreenState();
}

class _TodoListScreenState extends State<TodoListScreen>{

  final List<Todo> todos = [];

  void _toggleTodo(bool isChecked, Todo todo){
    setState((){
      todo.isDone = isChecked;
    });
  }

  _addTodo() async{
    final newTodo = await showDialog<Todo>(
      context : context,
      builder : (BuildContext context){
        return NewTodoDialog();
      }
    );
    if(newTodo != null){
      setState((){
        todos.add(newTodo);
      });
    }
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar : AppBar(title : Text('Todo in Flutter')),
        body : TodoList(
          todos : todos,
          onTodoToggle : _toggleTodo
        ),
        floatingActionButton : FloatingActionButton(
          child : Icon(Icons.add),
          onPressed : _addTodo
        )
      );
  }
}